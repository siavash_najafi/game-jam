using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.AI;

public class PlayerPresenter : MonoBehaviour
{

    [SerializeField] private KeyCode upKeyCode;
    [SerializeField] private KeyCode downKeyCode;
    [SerializeField] private KeyCode rightKeyCode;
    [SerializeField] private KeyCode leftKeyCode;


    [SerializeField] private KeyCode attackKeyCode;
    [SerializeField] private KeyCode defenceKeyCode;
    [SerializeField] private KeyCode bombKeyCode;

    [SerializeField] private MoveController moveController;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Rigidbody2D myRigidbody;
    [SerializeField] private float movementSpeed;
    [SerializeField] private Vector2 minMapLimit;
    [SerializeField] private Vector2 maxMapLimit;

    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private bool isAlivePlayer;


    Vector2 currentDirection = new Vector2();

    [SerializeField] private float defaultHP = 5;

    public float HP { get; private set; }


    private void Start()
    {
        HP = defaultHP;
    }

    public Action<bool> PlayerDied;
    private void SetDirection()
    {
        currentDirection = Vector2.zero;
        if (Input.GetKey(upKeyCode))
        {
            currentDirection.y += 1;
        }
        if (Input.GetKey(downKeyCode))
        {
            currentDirection.y += -1;
        }
        if (Input.GetKey(rightKeyCode))
        {
            currentDirection.x += 1;
        }
        if (Input.GetKey(leftKeyCode))
        {
            currentDirection.x += -1;
        }
    }

    private void Update()
    {
        SetDirection();
    }

    private void Move()
    {
        Vector2 destenation = myRigidbody.position + (currentDirection * movementSpeed);
        destenation = new Vector3(Mathf.Clamp(destenation.x, minMapLimit.x, maxMapLimit.x), Mathf.Clamp(destenation.y, minMapLimit.y, maxMapLimit.y), 0);
        moveController.Move(agent, myRigidbody, destenation, movementSpeed, movementSpeed);
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.LogError("OnCollisionEnter2D : " + collision.transform.tag);
    }

    private IEnumerator DamageEffect()
    {
        var defaultColor = spriteRenderer.color;
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.2f);
        spriteRenderer.color = defaultColor;
    }

    public void Die()
    {
        StopAllCoroutines();
        gameObject.SetActive(false);
    }

    public void DoDamage(float damage)
    {
        HP -= damage;
        StopCoroutine(DamageEffect());
        StartCoroutine(DamageEffect());
        if (HP<=0)
        {
            PlayerDied?.Invoke(isAlivePlayer);
            Die();
        }
    }
}
