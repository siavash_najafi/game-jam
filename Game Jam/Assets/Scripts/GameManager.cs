using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class GameManager : MonoBehaviour, IGMEnemyProvider
{
    [SerializeField] private PlayerPresenter playerAlive;
    [SerializeField] private PlayerPresenter playerGost;
    [SerializeField] private EnemySpawner enemySpawnerAlive;
    [SerializeField] private EnemySpawner enemySpawnerGost;


    private bool gameOver;

    public event Action GameOver;

    private void Start()
    {
        playerAlive.PlayerDied += OnPlayerDied;
        playerGost.PlayerDied += OnPlayerDied;

    }
    private void OnDestroy()
    {
        playerAlive.PlayerDied -= OnPlayerDied;
        playerGost.PlayerDied -= OnPlayerDied;
    }

    private void OnPlayerDied(bool isAlivePlayer)
    {
        GameOver?.Invoke();
        Debug.LogError("Player Died "+ isAlivePlayer);
        gameOver = true;
    }

    public void RequestDamage(bool isAlivePlayer, float damage)
    {
        if (gameOver )
            return;

        var targetPlayer = isAlivePlayer ? playerAlive : playerGost;
        targetPlayer.DoDamage(damage);
    }
}

public interface IGMEnemyProvider
{
    public void RequestDamage(bool isAlivePlayer,float damage);
    public event Action GameOver;
}
