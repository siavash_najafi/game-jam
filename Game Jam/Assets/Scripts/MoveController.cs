using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveController : MonoBehaviour
{
    [SerializeField] float stoppingDistance = 0.5f;

    public Vector3 CurrentMotion { get; protected set; }

    public virtual void Move(Transform movingBody, Vector3 motion)
    {
        CurrentMotion = motion;
        movingBody.position += motion;
    }

    public virtual void Warp(NavMeshAgent agent, Vector3 motion)
    {
        CurrentMotion = motion;
        Debug.LogError(agent.Warp(agent.transform.position + motion));
    }

    public virtual void Move(Transform movingBody, Vector3 destination, float maxDistanceDelta)
    {
        var moveVec = destination - movingBody.position;
        Move(movingBody, Vector3.MoveTowards(Vector3.zero, moveVec, maxDistanceDelta));
    }

    public virtual void Move(Rigidbody rigidbody, Vector3 velocity)
    {
        velocity.y = rigidbody.velocity.y;
        rigidbody.velocity = velocity;
        CurrentMotion = velocity;
    }

    public virtual void Move(Rigidbody2D rigidbody, Vector2 velocity)
    {
        rigidbody.velocity = velocity;
        CurrentMotion = velocity;
    }

    public virtual void Move(NavMeshAgent agent, Vector3 destination)
    {
        if (!agent.updatePosition)
        {
            agent.updatePosition = true;
            agent.updateRotation = true;
        }
        agent.SetDestination(destination);
    }

    public virtual void Move(NavMeshAgent agent, Vector3 destination, float maxDistanceDelta, float maxAngleData, bool updateRotation = true)
    {
        if (agent.updatePosition)
        {
            agent.updatePosition = false;
            agent.updateRotation = false;
        }
        agent.destination = destination;
        var motion = Vector3.MoveTowards(Vector3.zero, agent.steeringTarget - agent.transform.position, maxDistanceDelta);
        Move(agent.transform, motion);
        if (updateRotation && motion != Vector3.zero)
        {
            var directionToNextPoint = motion.normalized;
            directionToNextPoint.y = 0;
            RotateTo(agent.transform, directionToNextPoint, maxAngleData);
        }
        agent.nextPosition = agent.transform.position;
    }

    public virtual void Move(NavMeshAgent agent, Rigidbody rigidbody, Vector3 destination, float maxDistanceDelta, float maxAngleData, bool updateRotation = true)
    {
        if (agent.updatePosition)
        {
            agent.updatePosition = false;
            agent.updateRotation = false;
        }
        agent.destination = destination;
        Vector3 motion = agent.steeringTarget - agent.transform.position;
        var targetVec = destination - agent.transform.position;
        if (targetVec.magnitude < stoppingDistance)
            maxDistanceDelta = Mathf.Min(targetVec.magnitude, maxDistanceDelta);
        motion = motion.normalized * maxDistanceDelta;
        Move(rigidbody, motion);
        if (updateRotation && motion != Vector3.zero)
        {
            var directionToNextPoint = motion.normalized;
            directionToNextPoint.y = 0;
            RotateTo(rigidbody, directionToNextPoint, maxAngleData);
        }
        agent.nextPosition = agent.transform.position;
    }

    public virtual void Move(NavMeshAgent agent, Rigidbody2D rigidbody, Vector2 destination, float maxDistanceDelta, float maxAngleData)
    {
        if (agent.updatePosition)
        {
            agent.updatePosition = false;
            agent.updateRotation = false;
        }
        agent.destination = destination;
        Vector2 motion = new Vector2(agent.steeringTarget.x, agent.steeringTarget.y) - new Vector2(agent.transform.position.x, agent.transform.position.y);
        var targetVec = destination - new Vector2(agent.transform.position.x, agent.transform.position.y);
        if (targetVec.magnitude < stoppingDistance)
            maxDistanceDelta = Mathf.Min(targetVec.magnitude, maxDistanceDelta);
        motion = motion.normalized * maxDistanceDelta;
        Move(rigidbody, motion);
        agent.nextPosition = agent.transform.position;
    }

    public virtual void RotateTo(Transform rotatingBody, Vector3 direction, float maxAngleDelta)
    {
        rotatingBody.forward = Vector3.RotateTowards(rotatingBody.forward, direction, maxAngleDelta, 100);
    }

    public virtual void RotateTo(Rigidbody rigidbody, Vector3 direction, float maxAngleDelta)
    {
        rigidbody.MoveRotation(Quaternion.RotateTowards(rigidbody.rotation, Quaternion.LookRotation(direction, Vector3.up), maxAngleDelta));
    }

    public virtual void RotateTo(Rigidbody2D rigidbody, Vector3 direction, float maxAngleDelta)
    {
        //rigidbody.MoveRotation(Quaternion.RotateTowards(rigidbody.rotation, Quaternion.LookRotation(direction, Vector3.up), maxAngleDelta));
    }

    public bool HasRaeched(Vector3 currentPos, Vector3 des, float stoppingDst = 0.5f) => Vector3.Distance(currentPos, des) <= Mathf.Max(stoppingDst, stoppingDistance);
    public bool HasRaeched(Vector3 des, float stoppingDst = 0.5f) => HasRaeched(transform.position, des, stoppingDst);
    public bool HasRaeched(Vector3 currentPos, Vector3 des) => Vector3.Distance(currentPos, des) <= stoppingDistance;
    public bool HasRaeched(Vector3 des) => HasRaeched(transform.position, des, stoppingDistance);
}
