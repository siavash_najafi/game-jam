using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private MoveController moveController;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Rigidbody2D myRigidbody;
    [SerializeField] private float movementSpeed;
    [SerializeField] private Vector2 minMapLimit;
    [SerializeField] private Vector2 maxMapLimit;


    [SerializeField] private Transform targetPlayerT;
    [SerializeField] private float stopingDistance = 0.5f;

    [SerializeField] private float attackDistance = 1.5f;
    [SerializeField] private float attackDuration = 0.8f;

    private IGMEnemyProvider gameManager;

    [SerializeField] private bool isAliveEnemy;
    [SerializeField] private float damage = 1;

    private bool attackStarted;
    private bool isAttackValid =true;



    private void Start()
    {
        gameManager.GameOver += GameManager_GameOver;
    }

    private void OnDestroy()
    {
        gameManager.GameOver -= GameManager_GameOver;
    }
    private void GameManager_GameOver()
    {
        this.enabled = false;
    }

    private void Update()
    {
        var distanceFromPlayer = Vector3.Distance(targetPlayerT.position, transform.position);
        if (distanceFromPlayer > stopingDistance)
        {
            Move();
        }
        if (distanceFromPlayer <= attackDistance)
        {
            if (!attackStarted)
            {
                StartAttack();
            }
        }
        else
        {
            if (attackStarted)
            {
                StopExistingAttack();
            }
        }
    }
    private void StopExistingAttack()
    {
        attackStarted = false;
        isAttackValid = false;
        StopCoroutine(StartAttackTimer(attackDuration));
    }
    private void StartAttack()
    {
        Debug.LogError("Attack");
        attackStarted = true;
        isAttackValid = true;
        StopCoroutine(StartAttackTimer(attackDuration));
        StartCoroutine(StartAttackTimer(attackDuration));
    }
    private IEnumerator StartAttackTimer(float delay) {

        yield return new WaitForSeconds(delay);
        if (isAttackValid)
        {
            gameManager.RequestDamage(isAliveEnemy, damage);
        }
        attackStarted = false;
        isAttackValid = false;
    }

    private void Move()
    {
        Vector2 destenation = targetPlayerT.position;
        destenation = new Vector3(Mathf.Clamp(destenation.x, minMapLimit.x, maxMapLimit.x), Mathf.Clamp(destenation.y, minMapLimit.y, maxMapLimit.y), 0);
        moveController.Move(agent, myRigidbody, destenation, movementSpeed, movementSpeed);
    }

    public EnemyController Creat(Transform parent , Transform player,IGMEnemyProvider gMEnemyProvider) {

        var gameObject = Instantiate(this, parent);
        var controller  = gameObject.GetComponent<EnemyController>();
        controller.targetPlayerT = player;
        controller.gameManager = gMEnemyProvider;
        return controller;
    }
}
