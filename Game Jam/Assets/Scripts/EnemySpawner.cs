using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private EnemyController enemyController;
    [SerializeField] private float enemySpawnRate =1;
    [SerializeField] private Transform player;
    [SerializeField] private Transform enemiesParent;
    [SerializeField] private GameManager gameManager;

    private List<EnemyController> enemies = new List<EnemyController>();

    private float lastSpawnTime;
    private bool gameOver;

    private void Start()
    {
        gameManager.GameOver += GameManager_GameOver;
    }
    private void OnDestroy()
    {
        gameManager.GameOver -= GameManager_GameOver;
    }
    private void GameManager_GameOver()
    {
        this.enabled = false;
    }

    private void Update()
    {
        if (Time.time - lastSpawnTime >= enemySpawnRate)
        {
            SpawnEnemy();
        }
    }

    [ContextMenu("SpawnEnemy")]
    private void SpawnEnemy()
    {
        var enemy = enemyController.Creat(enemiesParent, player, gameManager);
        enemy.gameObject.SetActive(true);
        enemies.Add(enemy);
        lastSpawnTime = Time.time;
    }
}
